import { Component, EventEmitter, Output, OnInit, Inject, forwardRef } from '@angular/core';
import { Destino } from './../../models/destino.model';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
   selector: 'app-form-destino-viaje',
   templateUrl: './form-destino-viaje.component.html',
   styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
   @Output() onItemAdded: EventEmitter<Destino>;
   fg: FormGroup;
   minLongitud = 7;
   searchResults: string[];
   // prueba: string[];

   constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
      this.onItemAdded = new EventEmitter();
      // this.prueba = ['asdads', 'asdads', 'asdasdasd'];
      this.fg = fb.group({
      nombre: ['', Validators.compose([
    Validators.required,
    this.nombreValidator,
    this.nombreValidatorParametrizable(this.minLongitud)
 ])],
 url: [''],
 comentario: ['']
      });

      this.fg.valueChanges.subscribe(
 (form: any) => {
    console.log('form cambió:', form);
 }
 );

      this.fg.controls.nombre.valueChanges.subscribe(
    (value: string) => {
       console.log('nombre cambió:', value);
    }
    );
 }

ngOnInit() {
    const elemNombre =  <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
    .pipe(
       map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
       filter(text => text.length > 3),
       debounceTime(200),
       distinctUntilChanged(),
       switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
       ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
       // switchMap( () => this.prueba )//() => ajax('/assets/datos.json'))
       // ).subscribe(ajaxResponse => {
       // console.log(ajaxResponse);
       // console.log(ajaxResponse.response);
       // this.searchResults = this.prueba; // ajaxResponse.response;
       // });
    }

guardar(nombre: string, url: string, comentario: string) : boolean{
       const d = new Destino(nombre, url, comentario);
       this.onItemAdded.emit(d);
       return false;
    }



nombreValidator(control: FormControl) : {[s: string] : boolean; } {
       const l = control.value.toString().trim().length;
       if (l >= 0 && l < 5) {
  return { invalidNombre: true};
       }
       return null;
    }

nombreValidatorParametrizable(minLong: number) : ValidatorFn {
       return(control: FormControl): {[key: string]: boolean} | null => {
  const l = control.value.toString().trim().length;
  if (l < minLong) {
     return { minLongNombre: true};
  }
  return null;
       };
    }
 }
