import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import {Destino} from './../../models/destino.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store, State } from '@ngrx/store';
import { AppState } from '../../app.module';
import { Action } from 'rxjs/internal/scheduler/Action';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  // destinos : Destino [];
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onItemAdded: EventEmitter<Destino>;
  updates: string[];
  all;

  constructor(private destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito).subscribe(data => {
      const fav = data;
      if (data != null) {
        this.updates.push('Se elegio ' + data.nombre);
      }
    });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
    // this.destinosApiClient.subscribeOnChange((d: Destino) => {
    //   if (d != null) {
    //     this.updates.push('Se elegio ' + d.nombre);
    //   }
    // });
  }

  ngOnInit() {
  }

  /*guardar(nombre: string, url: string, comentario: string): boolean{
  	this.destinos.push(new Destino(nombre, url, comentario));
  	console.log(new Destino(nombre, url, comentario));
  	console.log(this.destinos);
    return false;
    let d = new DestinoViaje(nombre, url);
    this.destinos.push(d);
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    return false;
  }*/

  agregado(d: Destino) {
  this.destinosApiClient.add(d);
  this.onItemAdded.emit(d);
  // this.store.dispatch(new NuevoDestinoAction(d));
  /*elegido(d: Destino){
      this.destinos.forEach(function(x){x.setSelected(false); });
        d.setSelected(true);
    }*/
  }


  /*elegido(d: Destino){
    this.destinos.forEach(function(x){x.setSelected(false); });
      d.setSelected(true);
  }*/
  elegido(d: Destino) {
    this.destinosApiClient.elegir(d);
    // this.store.dispatch(new ElegidoFavoritoAction(d));
  }

  // getAll(){
    
  // }
}
