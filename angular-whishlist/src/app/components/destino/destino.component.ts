import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import {Destino} from './../../models/destino.model';
import { AppState } from '../../app.module';
import { Store} from '@ngrx/store';
import { VoteUpAction, VoteDownAction, VoteResetAction } from '../../models/destinos-viajes-state.model';
import {trigger, state, style, transition, animate} from '@angular/animations';

@Component({
  selector: 'app-destino',
  templateUrl: './destino.component.html',
  styleUrls: ['./destino.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})
export class DestinoComponent implements OnInit {
  @Input() destino: Destino;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter <Destino>;
  constructor(private store: Store<AppState>) {
      this.clicked = new EventEmitter();
  }
  ngOnInit() {
  }
  ir() {
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
  voteReset() {
    this.store.dispatch(new VoteResetAction(this.destino));
    return false;
  }

}
