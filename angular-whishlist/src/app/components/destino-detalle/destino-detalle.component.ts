import { Component, OnInit } from '@angular/core';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { InjectionToken, Inject } from '@angular/core';
import { Destino } from './../../models/destino.model';
import { ActivatedRoute } from '@angular/router';
// import { Store } from '@ngrx/store';
// import { AppState } from 'src/app/app.module';
// class DestinosApiClientViejo {
//   getById(id: string): Destino {
//     console.log('llamando clase vieja');
//     return null;
//   }
// }


// interface AppConfig {
//   apiEndpoint: string;
// }
// const APP_CONFIG_VALUE: AppConfig = {
//   apiEndpoint: 'mi_api.com'
// };
// const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

// class DestinosApiClientDecorated extends DestinosApiClient {
//   constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
//     super(store);
//   }
//   getById(id: string): Destino {
//     console.log('llamando por la clase decorada!');
//     console.log('config: ' + this.config.apiEndpoint);
//     return super.getById(id);
//   }
// }
@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [DestinosApiClient]
  //   { provide: DestinosApiClient, useClass: DestinosApiClientDecorated },
  //   { provide: DestinosApiClientViejo, useExisting: DestinosApiClient },
  //   { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE }
})
export class DestinoDetalleComponent implements OnInit {
  destino: Destino;
  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
  }

}
